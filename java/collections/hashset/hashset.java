import java.util.HashSet;
import java.util.Iterator;

class CollectionHashSet {
    public static void main(String[] args) {
        HashSet<Integer> numeros = new HashSet<>();
        numeros.add(2);
        numeros.add(5);
        numeros.add(8);

        System.out.println("numeros" + numeros);

        Iterator<Integer> it = numeros.iterator();
        while(it.hasNext()) {
            System.out.println("it: " + it.next());
        }
    }
}