import java.util.TreeSet;

class CollectionTreeset {
    public static void main(String[] args) {
        TreeSet<Integer> numeros = new TreeSet<>();
        numeros.add(8);
        numeros.add(6);
        numeros.add(9);
        System.out.println("numeros: " + numeros);

        boolean removeu = numeros.remove(61);
        System.out.println("removeu o 61: " + removeu);
        
        removeu = numeros.remove(6);
        System.out.println("removeu o 6: " + removeu);

        System.out.println("numeros: " + numeros);

        removeu = numeros.removeAll(numeros);
        System.out.println("removeu todos: " + removeu);

    }
}